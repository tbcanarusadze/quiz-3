package com.example.quiz3

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<Competitions.Area>, private val context: Context) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: Competitions.Area

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            model = items[adapterPosition]
            itemView.name.text = model.name
            itemView.countryCode.text = model.countryCode
            itemView.idTextView.text = model.id.toString()

            if(model.ensignUrl.isNullOrEmpty()){
                itemView.imageView.setImageResource(R.mipmap.nothing)
            }else{
                GlideToVectorYou.justLoadImage(context as Activity, Uri.parse(model.ensignUrl), itemView.imageView)

            }
        }
    }
}