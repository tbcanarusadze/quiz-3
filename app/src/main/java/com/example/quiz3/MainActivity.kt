package com.example.quiz3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz3.HttpRequest.COMPETITIONS
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private val competitionsList = mutableListOf<Competitions.Area>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        request(COMPETITIONS)
    }


    private fun init() {
        adapter = RecyclerViewAdapter(competitionsList, this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        loadingTextView.visibility = View.VISIBLE

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                request(COMPETITIONS)

            }, 2000)
        }

    }

    private fun refresh() {
        competitionsList.clear()
        adapter.notifyDataSetChanged()
        loadingTextView.visibility = View.VISIBLE
    }

    private fun request(path: String) {
        HttpRequest.getRequest(path, object : CustomCallback {
            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String) {
                parseJson(response)
                adapter.notifyDataSetChanged()
                loadingTextView.visibility = View.GONE

            }
        })
    }


    private fun parseJson(response: String) {

        val competitions = Competitions()
        val json = JSONObject(response)

        if (json.has("competitions")) {
            val jsonArray = json.getJSONArray("competitions")
            for (i in 0 until jsonArray.length()) {
                val mObject = jsonArray.getJSONObject(i)
                if (mObject.has("area")) {
                    val jsonObject = mObject.getJSONObject("area")
                    val area = Competitions.Area()
                    if (!jsonObject.isNull("id")) area.id = jsonObject.getInt("id")
                    if (!jsonObject.isNull("name")) area.name = jsonObject.getString("name")
                    if (!jsonObject.isNull("countryCode")) area.countryCode =
                        jsonObject.getString("countryCode")
                    if (!jsonObject.isNull("ensignUrl")) area.ensignUrl =
                        jsonObject.getString("ensignUrl")
                    competitions.area.add(area)
                    competitionsList.add(area)
                }
            }
        }

    }
}

